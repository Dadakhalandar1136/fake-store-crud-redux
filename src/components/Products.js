import React from "react";
import { Link } from "react-router-dom";
import './Products.css';
import { connect } from "react-redux";


class Products extends React.Component {

    getProductId = (id) => {
        const findProductId = this.props.cartProducts.find((product) => {
            return product.id === id;
        })

        return findProductId;

    }

    handleAddToCart = (product) => {
        const findProductId = this.getProductId(product.id);
        if (findProductId === undefined) {
            const cartProduct = {
                ...product,
                quantity: 1
            }
            this.props.addProductToCart(cartProduct);
        }
    }

    render() {
        return (
            this.props.products.map((product) => (
                <li key={product.id} >
                    <div className="main-container-wrapper">
                        <div className="update-icon">
                            <img src={product.image} alt={product.title}></img>
                        </div>
                        <div className="content-container">
                            <h2>{product.title}</h2>
                            <h4>{product.category}</h4>
                            <h3>${product.price}</h3>
                            <div className="rating">
                                <p>Rating: {product.rating.rate}</p>
                                <span>{"(" + product.rating.count + ")"}</span>
                            </div>
                            <p>{product.description}</p>
                        </div>
                        <div className="button-section">
                            <Link to={`/product/${product.id}`}>
                                <button className="update-btn"> update <i className="fa-solid fa-pen-to-square"></i>
                                </button>
                            </Link>
                            <button className="add-to-cart" onClick={() => {
                                this.handleAddToCart(product)
                            }}>
                                {this.getProductId(product.id) ? "Added" : "Add to cart"}
                            </button>
                            <button className="delete-btn" onClick={() => {
                                this.props.deleteProduct(product.id)
                            }}>
                                delete <i className="fa-solid fa-trash"></i>
                            </button>
                        </div>
                    </div>
                </li>
            ))
        )
    }
}

const mapStateToProps = (state) => {
    return {
        cartProducts: state.CartReducer.list
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        deleteProduct: (productId) => dispatch({
            type: "DELETE_PRODUCT",
            payload: productId
        }),

        addProductToCart: (product) => dispatch({
            type: "ADD_PRODUCT_TO_CART",
            payload: product
        })

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Products);