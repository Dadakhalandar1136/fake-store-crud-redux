import React from "react";
import { Link } from "react-router-dom";
import "./Header.css";

function Header() {
    return (
        <header>
            <div className="header-container-one">
                <Link to="/">
                    <h1>Fake Store</h1>
                </Link>
                <div className="wrapper">
                    <Link to="/">
                        <button className="home">
                            Home
                        </button>
                    </Link>
                    <Link to="/new-product">
                        <button className="new-product-button">
                            New Product
                        </button>
                    </Link>
                    <Link to="/cart">
                        <button className="cart-button">
                            Add Cart
                        </button>
                    </Link>
                </div>

            </div>
        </header>
    );
}


export default Header;