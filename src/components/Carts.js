import React, { Component } from "react";
import { Link } from "react-router-dom";
import Cart from "./Cart";
import './Cart.css';
import { connect } from "react-redux";


class Carts extends Component {
    constructor(props) {
        super(props);

        this.API_STATES = {
            LOADING: "loading",
            LOADED: "loaded",
        }

        this.state = {
            status: this.API_STATES.LOADING,
            errorMessage: "",
        }

    }

    componentDidMount = () => {
        this.setState({
            status: this.API_STATES.LOADED
        });
    }

    render() {

        return (
            <>
                {this.state.status === this.API_STATES.LOADED && this.props.products.length === 0 &&
                    <div className="no-data">
                        <h1>No products available at the moment. Please try again later.</h1>
                    </div>
                }
                {this.state.status === this.API_STATES.LOADED && this.props.products !== null &&
                    <div className="cart-wrapper">
                        <div className='main-container-body'>
                            <div className='main-right-container'>
                                <div className='header-container'>
                                    <h3>Items in your cart</h3>
                                    <p>(<span>{this.props.products.length}</span> )</p>
                                </div>
                            </div>
                            {
                                this.props.products.map((product) => {
                                    return (
                                        <Cart
                                            key={product.id}
                                            product={product}
                                        />
                                    )
                                })
                            }
                            <div className="footer-container">
                                <Link to="/" >
                                    <button className="shopping"><i className="fa-solid fa-arrow-left"></i> Continue shopping</button>
                                </Link>
                                <button className="checkout"><i className="fa-solid fa-cart-shopping"></i><span>Checkout</span></button>
                            </div>
                        </div>
                    </div>
                }
            </>
        );
    }
}

const mapStateToProps = (state) => {
    console.log(state.CartReducer.list)
    return {
        products: state.CartReducer.list
    }
}


export default connect(mapStateToProps)(Carts);