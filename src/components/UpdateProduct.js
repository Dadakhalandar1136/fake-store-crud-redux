import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

class UpdateProduct extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            product: {}
        }
    }

    handleClickUpdate() {

        const updatedProducts = {
            title: this.state.product.title,
            description: this.state.product.description,
            price: this.state.product.price,
            categroy: this.state.product.category,
            id: this.props.product.id,
            rating: this.props.product.rating,
            image: this.props.product.image
        }

        this.props.updateProduct(updatedProducts);
    }

    componentDidMount() {
        this.setState({
            product: this.props.product
        })

    }

    render() {
        // console.log("hello", this.state.product);
        return (
            <div className='row d-flex justify-content-center'>
                <div className='col-lg-8'>
                    <h1 className='d-flex justify-content-center h1 mt-4'>Update Product Details</h1>
                    <form>
                        <div className="form-group row m-3">
                            <label htmlFor="title" className="col-sm-2 col-form-label">Title</label>
                            <div className="col-sm-10">
                                <input
                                    type="text"
                                    className="form-control"
                                    id="title"
                                    value={this.state.product?.title}
                                    onChange={(event) => {
                                        this.setState({
                                            product: {
                                                ...this.state.product,
                                                title: event.target.value,
                                            }
                                        })
                                    }} />
                            </div>
                        </div>
                        <div className="form-group row m-3">
                            <label htmlFor="description" className="col-sm-2 col-form-label">Description</label>
                            <div className="col-sm-10">
                                <textarea
                                    className="form-control"
                                    rows="6"
                                    id="description"
                                    value={this.state.product?.description}
                                    onChange={(event) => {
                                        this.setState({
                                            product: {
                                                ...this.state.product,
                                                description: event.target.value,
                                            }
                                        })
                                    }} />
                            </div>
                        </div>
                        <div className="form-group row m-3">
                            <label htmlFor="price" className="col-sm-2 col-form-label">Price</label>
                            <div className="col-sm-10">
                                <input
                                    type="number"
                                    className="form-control"
                                    id="price"
                                    value={this.state.product?.price}
                                    onChange={(event) => {
                                        this.setState({
                                            product: {
                                                ...this.state.product,
                                                price: event.target.value,
                                            }
                                        })
                                    }} />
                            </div>
                        </div>
                        <div className="form-group row m-3">
                            <label htmlFor="category" className="col-sm-2 col-form-label">Category</label>
                            <div className="col-sm-10">
                                <input
                                    type="text"
                                    className="form-control"
                                    id="category"
                                    value={this.state.product?.category}
                                    onChange={(event) => {
                                        this.setState({
                                            product: {
                                                ...this.state.product,
                                                category: event.target.value,
                                            }
                                        })
                                    }} />
                            </div>
                        </div>
                        <div className='d-flex justify-content-center gap-4 mt-5 mb-4'>
                            <Link to="/">
                                <button type="button" className="btn btn-light px-5">Cancel</button>
                            </Link>
                            <Link to="/">
                                <button
                                    type="button"
                                    className="btn btn-success d-flex justify-content-center px-5"
                                    onClick={() => {
                                        this.handleClickUpdate();
                                    }}>Save</button>
                            </Link>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateProduct: (product) => dispatch({
            type: "UPDATE_PRODUCT",
            payload: product
        })
    }
}

export default connect(null, mapDispatchToProps)(UpdateProduct);