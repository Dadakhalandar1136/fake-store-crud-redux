import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

class Cart extends Component {

    increamentProduct = () => {
        this.props.increamentQuantity(this.props.product.id);
    }

    decrementProduct = () => {
        if (this.props.product.quantity > 1) {
            this.props.decrementQuantity(this.props.product.id);
        }
    }

    deleteProduct = () => {
        this.props.deleteProductFromCart(this.props.product.id);
    }

    render() {

        return (
            <div className="left-container">
                <div className="box">
                    <Link to={`/product/${this.props.product.id}`}>
                        <div className="item-thumbnail">
                            <img src={this.props.product.image} alt={this.props.product.title}></img>
                        </div>
                    </Link>
                    <div className="box-left">
                        <h1>{this.props.product.title}</h1>
                        <p>{this.props.product.description}</p>
                        <i className="fa-solid fa-gift"></i>
                        <span>Add gift package </span>
                    </div>
                    <div className="box-right">
                        <div className="final-ammount">
                            <div className="ammount-container">
                                <p>${this.props.product.price}</p>
                            </div>

                            <button className='minus' onClick={this.decrementProduct}>-</button>
                            <div className="quantity">
                                <p>{this.props.product.quantity}</p>
                            </div>
                            <button className='plus' onClick={this.increamentProduct}>+</button>

                        </div>
                        <h3>${((this.props.product.price) * (this.props.product.quantity)).toFixed(2)}</h3>
                    </div>
                </div>
                <div className='delete-container'>
                    <button className='delete-product' onClick={this.deleteProduct}>
                        <i className="fa-solid fa-trash"></i>
                    </button>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        increamentQuantity: (productId) => dispatch({
            type: "INCREAMENT_QUANTITY",
            payload: productId
        }),

        decrementQuantity: (productId) => dispatch({
            type: "DECREMENT_QUANTITY",
            payload: productId
        }),

        deleteProductFromCart: (productId) => dispatch({
            type: "DELETE_PRODUCT_FROM_CART",
            payload: productId
        })
    }
}

export default connect(null, mapDispatchToProps)(Cart);