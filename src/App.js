import React from 'react';
import axios from 'axios';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';
import Products from './components/Products';
import Loader from './components/Loader';
import CreateProduct from './components/CreateProduct';
import Header from './components/Header';
import UpdateProduct from './components/UpdateProduct';
import { Switch } from 'react-router-dom/cjs/react-router-dom.min';
import { connect } from 'react-redux';
import Carts from './components/Carts';

class App extends React.Component {

  constructor(props) {
    super(props);

    this.API_STATES = {
      LOADING: "loading",
      LOADED: "loaded",
      ERROR: "error",
    }

    this.state = {
      status: this.API_STATES.LOADING,
      errorMessage: "",
    }

    this.URL = 'https://fakestoreapi.com/products/';
  }

  fetchData = (url) => {
    this.setState({
      status: this.API_STATES.LOADING,
    }, () => {
      axios.get(url)
        .then((response) => {
          this.props.initialProducts(response.data);
          this.setState({
            status: this.API_STATES.LOADED,
          })
        })
        .catch((err) => {
          this.setState({
            status: this.API_STATES.ERROR,
            errorMessage: "An API error occured. Please try again in a few minutes.",
          })
        })
    })
  }

  componentDidMount = () => {
    this.fetchData(this.URL);
  }

  render() {

    return (
      <Router>
        <div className="App">
          <Header />
          {this.state.status === this.API_STATES.ERROR &&

            <div className='fetch-fail'>
              <h2> {this.state.errorMessage} </h2>
            </div>
          }

          {this.state.status === this.API_STATES.LOADING &&
            <Loader />
          }

          {this.state.status === this.API_STATES.LOADED && this.props.products.length === 0 &&
            <div className="no-data">
              <h2>No products available at the moment. Please try again.</h2>
            </div>
          }

          {this.state.status === this.API_STATES.LOADED && this.props.products.length > 0 &&
            <Switch >
              <Route path="/" exact>
                <div className="main-coontainer">
                  <ul>
                    <Products products={this.props.products}
                      handleClickDelete={this.handleClickDelete} />
                  </ul>
                </div>
              </Route>
              <Route path="/new-product">
                <CreateProduct handleClickCreate={this.handleClickCreate} />
              </Route>
              <Route exact path="/cart">
                <Carts />
              </Route>
              <Route path="/product/:id" render={(routeProps) => {
                return <UpdateProduct
                  id={routeProps.match.params.id}
                  product={this.props.products.find((product) => {
                    return String(product.id) === (routeProps.match.params.id);
                  })}
                  handleClickUpdate={this.handleClickUpdate} />
              }} />
            </Switch>
          }
        </div>
      </Router>
    );
  }
}

const mapStateToProps = (state) => {
  // console.log("hello", state.ProductsReducer.list);
  return {
    products: state.ProductsReducer.list
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    initialProducts: (products) => dispatch({
      type: 'INITIAL_PRODUCTS',
      payload: products
    })
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(App);
