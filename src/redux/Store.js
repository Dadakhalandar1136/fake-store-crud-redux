import { configureStore } from '@reduxjs/toolkit';

import ProductsReducer from './reducers/ProductsReducer';
import CartReducer from './reducers/CartReducer';

const reducer = {
    ProductsReducer,
    CartReducer,
}

export default configureStore({
    reducer
});