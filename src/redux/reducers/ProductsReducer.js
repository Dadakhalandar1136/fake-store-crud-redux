import { ADD_PRODUCT, DELETE_PRODUCT, UPDATE_PRODUCT, INITIAL_PRODUCTS } from "../ActionTypes";

const initialState = {
    list: [],
}

export default function productAction(state = initialState, action) {
    switch (action.type) {
        case INITIAL_PRODUCTS: {
            return {
                ...state,
                list: action.payload
            }
        }

        case ADD_PRODUCT: {
            return {
                ...state,
                list: [
                    ...state.list,
                    action.payload
                ]
            }
        }

        case UPDATE_PRODUCT: {
            return {
                ...state,
                list: state.list.map((presentProduct) => {
                    return presentProduct.id === action.payload.id ? action.payload : presentProduct;
                })
            }
        }

        case DELETE_PRODUCT: {
            return {
                ...state,
                list: state.list
                    .filter((presentProduct) => {
                        return presentProduct.id !== action.payload;
                    })
            }
        }

        default: {
            return state;
        }
    }
}